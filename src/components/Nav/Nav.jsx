import React from "react";

import {titleOpacity} from "../Title/Title"

import "./style.css"

export const addanim = ()=>{
    const [...arrItem] = document.querySelectorAll(".nav-item")
    const length = arrItem.length;
    let index = 0;
  
    setInterval(()=>{
        if(index<length){
            if(index === length-1){
                arrItem[index].addEventListener("animationend", (e)=>{
                    setInterval(titleOpacity, 1000)
                })
            }
            arrItem[index].classList.add("animate__animated", "animate__bounceInDown");
            index++;
        }
        
    },500)
   
}
export const Nav = () =>{
    return(
        <div className="nav-menu">
            <div className="nav-item">Inicio</div>
            <div className="nav-item">Espacio</div>
            <div className="nav-item">Planetas</div>
            <div className="nav-item">Nosotros</div>
            <div className="nav-item">Misiones</div>
        </div>
    )
}
