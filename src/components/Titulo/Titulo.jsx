import React from "react";

import "./style.css";

export const tituloMove = () => {
    const titulo = document.querySelector(".titulo-container")
    titulo.classList.add("titulo-active")
}

export const Titulo = () => {
  return (
    <div className="titulo-container">
      <div className="titulo-title">Titulo</div>
      <div className="titulo-subtitle">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Semper viverra
        tempor, enim vulputate nunc interdum sit diam ultrices. Sed erat
        volutpat curabitur ornare in facilisi ornare. Vitae mollis sed feugiat
        ipsum condimentum eget magnis nulla at. Massa semper massa quisque
        tincidunt cursus. Elementum aliquet sed lectus facilisis massa in. Felis
        lectus egestas urna egestas arcu. Quam quisque volutpat lacus, eget.
        Quis risus, rhoncus nisi a, sit libero ut viverra. Magna quis hendrerit
        in cursus. Sed sed vitae ullamcorper dignissim tristique. Imperdiet
        vulputate blandit eu egestas massa a mauris libero. Mi turpis sagittis
        ac elit id sollicitudin urna. Velit neque neque vitae ultrices sagittis
        hendrerit in cursus. Sed egestas commodo mi sed. Aliquam at nunc,
        vestibulum viverra ipsum. Libero scelerisque tortor pellentesque ante ut
        sit nunc, vitae. ulla donec ultrices quis eu adipiscing habitant.
      </div>
    </div>
  );
};
