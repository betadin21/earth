import React, { Component } from "react";

import earth from "../../icons/earth.png";
import { addanim } from "../Nav/Nav";

import "./style.css";


class Earth extends Component {
    state = {
        cl : "earth-img",
        anticlock : "earth-rotate-anticlockwise",
        clock : "earth-rotate-clockwise",
        zoom : "zoom-img"
    }
    changeClass = (img) =>{
            if(new Date().getMinutes() % 2 === 0){
                img.classList.add(this.setState((state)=>{
                    return {
                        ...state,
                        cl : this.state.clock,
                    }
                }))
            }else{
                img.classList.add(this.setState((state)=>{
                    return {
                        ...state,
                        cl : this.state.anticlock,
                    }
                }))
            }
    }
    imgZoom = (img) =>{
        img.classList.add(this.setState((state)=>{
            return {
                ...state,
                cl : this.state.zoom,
            }
        }))
        img.addEventListener("animationend", (e)=>{
            addanim()
        })
    }
    pushEarthUp () {
        const img = document.querySelector(".zoom-img")
        img.classList.add("earthUp")
    }
  render() {
    return (
      <div className="eacth-img-container">
        <img className={this.state.cl } onLoad={(e)=>{this.changeClass(e.target)}} onClick={(e)=>{this.imgZoom(e.target)}} src={earth} alt="earth" />
      </div>
    );
  }
}

export default Earth;
