import React from "react";


import Earth from "../Earth/Earth";
import {Nav} from "../Nav/Nav";
import {Title} from "../Title/Title"
import { Arrow } from "../Arrow/Arrow";
import { Rocket } from "../Rocket/Rocket";
import { Titulo } from "../Titulo/Titulo";

import "./style.css";



const App = () => {
  return (
      <div className="wrapper">
        <Nav/>
        <Earth />
        <Title/>
        <Arrow/>
        <Rocket/>
        <Titulo/>
      </div>
  );
};

export default App;
