import React from "react";

import "./style.css";
import rocket from "../../icons/rocket.png"

export const moveRocket = () =>{
    const rocket = document.querySelector(".rocket-container")
    rocket.classList.add("rocket-active")
}

export const Rocket = () => {
  return (
    <div className="rocket-container">
      <img src={rocket} alt="rocket" className="rocket-img" />
    </div>
  );
};
