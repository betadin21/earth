import React from "react";

import Earth from "../Earth/Earth";
import { pushTitle } from "../Title/Title";
import { moveRocket } from "../Rocket/Rocket";
import { tituloMove } from "../Titulo/Titulo";

import "./style.css"

import arrow from "../../icons/arrow.svg"

export const showArrow = () =>{
    const arrow2 = document.querySelector(".arrow");
    arrow2.classList.add("active", "animate__animated", "animate__bounce", "animate__infinite");
    arrow2.addEventListener("click", ()=>{
        arrow2.classList.remove("active", "animate__animated", "animate__bounce", "animate__infinite")
        arrow2.style.transition = "none";
        arrow2.style.display = "none"
        Earth.prototype.pushEarthUp()
        pushTitle()
        setTimeout(moveRocket, 1000)
        setTimeout(tituloMove, 1000)

    })
}

export const Arrow = () => {
    return(
        <div className="arrow"><img src={arrow} alt="arrow" /> </div>
    )
}


    
