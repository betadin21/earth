import React from "react";

import { showArrow } from "../Arrow/Arrow";

import "./style.css"

export const titleOpacity = () => {
    const title = document.querySelector(".title")
    title.classList.add("active")
    title.addEventListener("animationend", ()=>{
       showArrow()
    })
}
export const pushTitle = () => {
    const title = document.querySelector(".title.active")
    title.classList.add("moveLeft")

}

export const Title = () =>{
    return(
        <div className="title">Un viaje al espacio</div>
    )
}

